//
//  SecondTabBarVC.h
//  Out N about
//
//  Created by Ram Kumar on 10/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllEventsTabBarVC : UIViewController<UITableViewDataSource,UITableViewDelegate,NSURLConnectionDataDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *imagesTableView;

@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@end
