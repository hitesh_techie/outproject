//
//  SingleEventVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 22/06/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <EventKit/EventKit.h>

@interface SingleEventVC : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;



@property(nonatomic,strong) NSString *singleTitle;
@property(nonatomic,strong) NSString *singleImageString;
@property(nonatomic,strong) NSString *singleCategoryString;
@property(nonatomic,strong) NSString *startDateOrTimeString;
@property(nonatomic,strong) NSString *endDateOrTimeString;
@property(nonatomic,strong) NSString *venueString;
@property(nonatomic,strong) NSString *costString;
@property(nonatomic,strong) NSString *descriptionString;
@property(nonatomic,strong) NSString *favoriteString;
@property(nonatomic,strong) NSString *viewsString;
@property(nonatomic,strong) NSString *eventIdString;
@property(nonatomic,strong) NSString *commentsString;
@property(nonatomic,strong) NSString *favsString;
@property(nonatomic,strong) NSString *userNameString;
@property(nonatomic,strong) NSString *userCommentString;




@property (weak, nonatomic) IBOutlet UILabel *singleTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *startDateOrTime;
@property (weak, nonatomic) IBOutlet UILabel *endDateOrTime;
@property (weak, nonatomic) IBOutlet UILabel *venueLabel;
@property (weak, nonatomic) IBOutlet UIButton *favButton;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UILabel *viewsLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCommentsLabel;
@property (weak, nonatomic) IBOutlet UILabel *favCountLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *noReviewLabel;
@property (weak, nonatomic) IBOutlet UITextView *userCommentSection;
@property (weak, nonatomic) IBOutlet UITextField *singleCatField;




@end
