//
//  FreeEventTabbarVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 30/06/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FreeEventTabbarVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *imagesTableView;

@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;

@end
