//
//  MainTabbarController.m
//  Out N about
//
//  Created by Ram Kumar on 11/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "MainTabbarController.h"

#import "ViewController.h"
#import "SWRevealViewController.h"
#import "LoginVC.h"

#import "AllEventsTabBarVC.h"
#import "PaidEventTabbarVC.h"
#import "TabBarTransitionController.h"
#import "TabBarControllerDelegate.h"
#import "ScrollTransition.h"
#import "SearchEventsVC.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface MainTabbarController ()


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs ((double)[[UIScreen mainScreen] bounds].size.height - (double) 667) < DBL_EPSILON)
@end

@implementation MainTabbarController
{
     NSMutableArray *imagesArray;
    NSString *access_token;
    NSMutableArray *titleLabel;
  //  MainTabbarController *mainTabbarObject;
 //   UITabBarController *tabBars;
 // ThirdTabbarVC *greenViewController;
}
@synthesize screenType;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    
    _barButtonItem = [[UIBarButtonItem alloc]init];
    [_barButtonItem setImage:[UIImage imageNamed:@"ic_menu@3x.png"]];
    _barButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = _barButtonItem;
    
    
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc]
                                   initWithImage:[UIImage imageNamed:@"ic_search@3x.png"]
                                   style:UIBarButtonItemStyleBordered
                                   target:self
                                   action:@selector(searchView)];
    searchButton.tintColor = [UIColor whiteColor];
    
//    UIBarButtonItem *filterButton = [[UIBarButtonItem alloc]
//                                     initWithImage:[UIImage imageNamed:@"menu-2-512.png"]
//                                     style:UIBarButtonItemStyleBordered
//                                     target:self
//                                     action:@selector(filterView)];
//    filterButton.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:searchButton/*,filterButton*/, nil];
    
    
    
    
    [[UITabBar appearance] setBarTintColor:[UIColor clearColor]];
    [[UITabBar appearance] setBackgroundImage:[UIImage new]];
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIFont fontWithName:@"HelveticaNeue-Medium" size:13.5f], UITextAttributeFont,
                                                           [UIColor whiteColor], UITextAttributeTextColor,
                                                           
                                                           [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 0.0f)], UITextAttributeTextShadowOffset,
                                                           nil] forState:UIControlStateNormal];
        
    }
    
    else
    {

    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIFont fontWithName:@"HelveticaNeue-Medium" size:13.2f], UITextAttributeFont,
                                                       [UIColor whiteColor], UITextAttributeTextColor,
                                                       
                                                       [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 0.0f)], UITextAttributeTextShadowOffset,
                                                       nil] forState:UIControlStateNormal];
    
    
    }
    
    
    [UITabBarItem.appearance setTitleTextAttributes:
     @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.98 green:0.94 blue:0.17 alpha:1.0]}
                                           forState:UIControlStateSelected];
    
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake([UITabBar appearance].frame.origin.x,[UITabBar appearance].frame.origin.y, [[UIScreen mainScreen] bounds].size.width/-4, 45)]; //49 is y axis of line and -6 is length width of line on both side L or R.
    
    UIImageView *border = [[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x,view1.frame.size.height-2, [[UIScreen mainScreen] bounds].size.width/2,30)]; // 30 is height of line
    border.backgroundColor = [UIColor colorWithRed:0.98 green:0.94 blue:0.17 alpha:1.0];
    [view1 addSubview:border];
    UIImage *img=[self ChangeViewToImage:view1];
    [[UITabBar appearance] setSelectionIndicatorImage:img];
    [[UITabBar appearance] setTintColor: [UIColor colorWithRed:0.973 green:0.733 blue:0.718 alpha:1]];
    
    
    
    

    
    self.delegate = self;
    
    UILabel* titleLabelNav = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
    titleLabelNav.textAlignment = NSTextAlignmentLeft;
    titleLabelNav.text = NSLocalizedString(@"All event",@"");
    titleLabelNav.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabelNav;

    //self.title = @"All event";
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};

    
    
    _barButtonItem.target = self.revealViewController;
    _barButtonItem.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    NSString *password = [defaults objectForKey:@"password"];
     access_token = [defaults objectForKey:@"access_token"];
    
    NSLog(@" access token in tab view %@",access_token );

    NSLog(@"username1: %@  pass1 :%@",username,password);
    
   


    if (access_token==nil) {
        NSLog(@"Access Token nil");
        
        }
    
    else{
     // [self postMethod];
    }
    
    
    
    
}



-(UIImage * ) ChangeViewToImage : (UIView *) viewForImage
{
    UIGraphicsBeginImageContext(viewForImage.bounds.size);
    [viewForImage.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}


- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    
    NSArray *tabViewControllers = tabBarController.viewControllers;
    UIView * fromView = tabBarController.selectedViewController.view;
    UIView * toView = viewController.view;
    if (fromView == toView)
        return false;
    NSUInteger fromIndex = [tabViewControllers indexOfObject:tabBarController.selectedViewController];
    NSUInteger toIndex = [tabViewControllers indexOfObject:viewController];
    
    [UIView transitionFromView:fromView
                        toView:toView
                      duration:0.3
                       options: toIndex > fromIndex ? UIViewAnimationOptionTransitionCrossDissolve : UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished) {
                        if (finished) {
                            tabBarController.selectedIndex = toIndex;
                        }
                    }];
    return true;
}


-(void)searchView
{
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SearchEventsVC *searchEvent = [story instantiateViewControllerWithIdentifier:@"searchView"];

    [self.navigationController pushViewController:searchEvent animated:YES];
    
    
}



-(void)filterView
{
    
}


//-(void)postMethod
//{
//    
//    
//    [self.connection cancel];
//    
//    //initialize new mutable data
//    NSMutableData *data = [[NSMutableData alloc] init];
//    self.receivedData = data;
//    
//    //initialize url that is going to be fetched.
//    NSURL *url = [NSURL URLWithString:@"http://agiledevelopers.in/outnabout/webservices/users/allevents"]; //http://192.168.1.22/outnabout/webservices/users/allevents/
//    
//    //initialize a request from url
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
//    
//    //set http method
//    [request setHTTPMethod:@"POST"];
//    //initialize a post data
//    NSString *postData = [NSString stringWithFormat:@"app_token=%@",access_token];
//    //   NSLog(@"username****: %@  pass**** :%@",usernameTF.text,passwordTF.text);
//    
//    //    NSString *postData = @"mac_id=val2&email_id=abc@gmail.com&password=abcfggkjhkj";
//    //Here you can give your parameters value
//    
//    //set request content type we MUST set this value.
//    
//    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
//    
//    //set post data of request
//    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    //initialize a connection from request
//    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    self.connection = connection;
//    
//    //start the connection
//    [connection start];
//    
//}





//#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
//
//-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
//    [self.receivedData appendData:data];
//}
//
//
//-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
//    
//    NSLog(@"eeeeeeee  %@" , error);
//}
//
//
//-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
//    
//    //initialize convert the received data to string with UTF8 encoding
//    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
//                                              encoding:NSUTF8StringEncoding];
//    // NSLog(@" ffff     %@" , htmlSTR);
//    
//    
//    NSError *error = nil;
//    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse
//    
//    //  NSLog(@"dataaa^^^^^^^^^^^^^^^^^^ %@",jsonArray);  //here is your output
//    
//  //  NSString *code = [jsonArray[@"code"] stringValue];
//    NSMutableArray *data = jsonArray[@"data"];
//    
//    
//    imagesArray = [[NSMutableArray alloc]init];
//    titleLabel = [[NSMutableArray alloc]init];
//
//    
//    for (NSDictionary *d  in  data) {
//        NSString *imageString = d[@"image"];
//        NSString *titleText = d[@"title"];
//        [imagesArray  addObject:imageString];
//
//        [titleLabel addObject:titleText];
//
//        
//      //  NSLog(@"image valuesss :%@",imageString);
//    }
//    
//    
//    
//    
//
//
//  //  [imagesArray addObject:[data valueForKey:@"image"]];
//    
//    
//    //NSLog(@"Images Data :%@",imagesArray);
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    [defaults setObject:imagesArray forKey:@"imageKey"];
//    [defaults setObject:titleLabel forKey:@"titleKey"];
//    [defaults synchronize];
//    
//    
//}




//- (void)viewWillLayoutSubviews
//{
//    [super viewWillLayoutSubviews];
//    
//    [self.tabBar invalidateIntrinsicContentSize];
//    
//    CGFloat tabSize = 100.0;
//    
//    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
//    
//    if (UIInterfaceOrientationIsLandscape(orientation))
//    {
//        tabSize = 32.0;
//    }
//    
//    CGRect tabFrame = self.tabBar.frame;
//    
//    tabFrame.size.height = tabSize;
//    
//    tabFrame.origin.y = self.view.frame.origin.y+10;
//    
//    self.tabBar.frame = tabFrame;
//    
//    // Set the translucent property to NO then back to YES to
//    // force the UITabBar to reblur, otherwise part of the
//    // new frame will be completely transparent if we rotate
//    // from a landscape orientation to a portrait orientation.
//    
//    self.tabBar.translucent = NO;
//    self.tabBar.translucent = YES;
//}








- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [self.tabBar invalidateIntrinsicContentSize];
    
    CGFloat tabSize = 25.0;
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    if (UIInterfaceOrientationIsLandscape(orientation))
    {
        tabSize = 32.0;
    }
    
    CGRect tabFrame = self.tabBar.frame;
    
    tabFrame.size.height = tabSize;
    
    if (IS_IPHONE_5) {
        tabFrame.origin.y = self.view.frame.origin.y+65;
       // tabFrame.origin.x = self.view.frame.origin.x-10;
    }
    else
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            
            tabFrame.origin.y = self.view.frame.origin.y+75;
            // tabFrame.origin.x = self.view.frame.origin.x-10;
            
        }
        else
        {
            tabFrame.origin.y = self.view.frame.origin.y+65;
        }
    
    
    
    self.tabBar.frame = tabFrame;
    
    self.tabBar.translucent = NO;
    self.tabBar.translucent = YES;
}







-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if ([screenType isEqualToString:@"LogoutNav"]) {
        NSLog(@"logout clicked");
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Message"
                                      message:@"You really want to logout."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                       FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                                       [loginManager logOut];
                                       
                                       [FBSDKAccessToken setCurrentAccessToken:nil];
                                       [FBSDKProfile setCurrentProfile:nil];
                                       
                                       
                                       
                                       FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                                     initWithGraphPath:@"me/permissions/" parameters:nil HTTPMethod:@"DELETE"];
                                       [request   startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                                           
                                           NSLog(@"Delete facebook login");
                                           
                                       }];
                                       
                                       
                                       [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];

                                       NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
                                       // [defs removeObjectForKey:@"username"];
                                       //    [defs removeObjectForKey:@"password"];
                                       NSDictionary * dict = [defs dictionaryRepresentation];
                                       for (id key in dict) {
                                           [defs removeObjectForKey:key];
                                       }
                                       [defs synchronize];
                                       
                                       
                                       
                                       [self resetDefaults];
                                       [self performSegueWithIdentifier:@"tologin" sender:nil];
                                       
                                                                              
                                       access_token=nil;
                                       // [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
                                       
                                       screenType = nil;
                                       
                                       
                                   }];
        UIAlertAction* cancelButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           //Handel your yes please button action here
                                           
                                           
                                       }];
        
        [alert addAction:okButton];
        [alert addAction:cancelButton];
        
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        int loggedIn = (int)[defaults integerForKey:@"isloggedIn"];
        if (loggedIn !=1)
        {
            [self performSegueWithIdentifier:@"tologin" sender:nil];
            
            NSLog(@"go to login view***** ");
        }
        else{
            //  [self performSegueWithIdentifier:@"toTabbar" sender:nil];
             NSLog(@"back to login view***** ");
            NSString *username = [defaults objectForKey:@"username"];
            _nameLabel.text = username;
        }
        
        
    }
    
}


- (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    // [defs removeObjectForKey:@"username"];
    //    [defs removeObjectForKey:@"password"];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}




//-(void)createTableView
//{
//    self.imagesTableView = [[UITableView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
//    
//    self.imagesTableView.delegate=self;
//    self.imagesTableView.dataSource=self;
//    self.imagesTableView.backgroundColor=[UIColor redColor];
//    // self.imagesTableView.rowHeight = IS_IPAD ? 60 : 50.0f;
//    self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.imagesTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
//    
//    //[self.view addSubview:self.imagesTableView];
//}




//#pragma mark - tableview delegate methods ================= ================ ================ ================ ================
//
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    
//    return  160;
//    
//}
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    
//    return garbageArray.count;
//    
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    if (cell == nil)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.textLabel.backgroundColor = [UIColor clearColor];
//    }
//    
//    cell.textLabel.text = [garbageArray objectAtIndex:indexPath.row];
//    UIView *_lineDiv=[[UIView alloc] init];
//    _lineDiv.frame=CGRectMake(0, 160.0f, self.view.frame.size.width, 1.0);
//    _lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
//    [cell addSubview:_lineDiv];
//    
//    return cell;
//    
//}



//- (void)viewWillLayoutSubviews
//{
//    [super viewWillLayoutSubviews];
//    
//    [self.tabBar invalidateIntrinsicContentSize];
//    
//    CGFloat tabSize = 54.0;
//    
//    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
//    
//    if (UIInterfaceOrientationIsLandscape(orientation))
//    {
//        tabSize = 32.0;
//    }
//    
//    CGRect tabFrame = self.tabBar.frame;
//    
//    tabFrame.size.height = tabSize;
//    
//    tabFrame.origin.y = self.view.frame.origin.y+10;
//    
//    self.tabBar.frame = tabFrame;
//    
//    // Set the translucent property to NO then back to YES to
//    // force the UITabBar to reblur, otherwise part of the
//    // new frame will be completely transparent if we rotate
//    // from a landscape orientation to a portrait orientation.
//    
//    self.tabBar.translucent = NO;
//    self.tabBar.translucent = YES;
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}


@end
