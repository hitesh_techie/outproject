//
//  FourthTabbarVC.m
//  Out N about
//
//  Created by Ram Kumar on 12/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "TopEventTabbarVC.h"
#import "SingleEventVC.h"
#import "SDWebImageManager.h"
#import "ALToastView.h"

@interface TopEventTabbarVC ()


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs ((double)[[UIScreen mainScreen] bounds].size.height - (double) 667) < DBL_EPSILON)

#define API_URL @"http://agiledevelopers.in/outnabout/webservices/users/"

@end

@implementation TopEventTabbarVC
{
    NSArray *garbageArray;
    UIRefreshControl *refresh;
    NSMutableArray *imageValue;
    NSMutableArray *imagesArray;
    NSMutableArray *venueArray;
    NSMutableArray *dateArray;
    NSMutableArray *startTimeArray;
    NSMutableArray *endTimeArray;
    NSMutableArray *eventIdArray;
    NSMutableArray *favoriteArray;
    NSMutableArray *eventViewsArray;
    NSMutableArray *costArray;
    NSMutableArray *descriptionArray;
    NSMutableArray *categoryArray;

    NSString *access_token;
     NSMutableArray *titleLabel;
    
    UIActivityIndicatorView *activityView;
    UIView* loadingView;
    UIImageView *favImage;
    CGRect rectText;
    CGRect rectFavImage;
    CGRect rectCatText;

    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    access_token = [defaults objectForKey:@"access_token"];
    
    NSLog(@" access token in tab view %@",access_token );
    
    
//    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(tappedRightButton:)];
//    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
//    [self.view addGestureRecognizer:swipeRight];

    
    refresh = [[UIRefreshControl alloc]init];
    refresh.backgroundColor = [UIColor blackColor];
    refresh.tintColor = [UIColor whiteColor];
    [refresh addTarget:self action:@selector(topEventRefreshTable) forControlEvents:UIControlEventValueChanged];
    [_imagesTableView addSubview:refresh];
    
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
   
    [self createTableView];
    [self postMethod];
    [_imagesTableView reloadData];
    
    loadingView = [[UIView alloc]initWithFrame:CGRectMake(100, 400, 80, 80)];
    loadingView.backgroundColor = [UIColor colorWithWhite:0. alpha:0.6];
    loadingView.center = self.view.center;
    loadingView.layer.cornerRadius = 5;
    
    activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.center = CGPointMake(loadingView.frame.size.width / 2.0, 35);
    [activityView startAnimating];
    activityView.tag = 100;
    [loadingView addSubview:activityView];
    
    UILabel* lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(0, 48, 80, 30)];
    lblLoading.text = @"Loading...";
    lblLoading.textColor = [UIColor whiteColor];
    lblLoading.font = [UIFont fontWithName:lblLoading.font.fontName size:15];
    lblLoading.textAlignment = NSTextAlignmentCenter;
    [loadingView addSubview:lblLoading];
    
    [self.view addSubview:loadingView];

 
}


-(void)topEventRefreshTable
{
    [refresh beginRefreshing];
    [self postMethod];
    [_imagesTableView reloadData];
    
    if (refresh) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        refresh.attributedTitle = attributedTitle;
        
        [ALToastView toastInView:self.view withText:@"Events Updated"];
    }
    
    [refresh endRefreshing];
}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_imagesTableView reloadData];
    
}


-(void)postMethod
{
    
    
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:(API_URL @"topevents")];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSString *postData = [NSString stringWithFormat:@"app_token=%@",access_token];
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    //set post data of request
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
    
}


#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"eeeeeeee  %@" , error);
}



-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //initialize convert the received data to string with UTF8 encoding
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse
    
    
    //  NSString *code = [jsonArray[@"code"] stringValue];
    NSMutableArray *data = jsonArray[@"data"];
    NSString *msg = jsonArray[@"msg"];
    if ([msg isEqualToString:@"Permission denied Invalid access token!"]) {
        [loadingView setHidden:YES];
        [[[UIAlertView alloc]initWithTitle:@"Warning!" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }else{
        
    }
    
    
   // NSMutableArray *categories = [jsonArray[@"data"] valueForKey:@"categories"];
    //NSLog(@"json data response *****    %@" , data);
    //  NSLog(@"json data response categry*****    %@" , categories);

    
    imagesArray = [[NSMutableArray alloc]init];
    titleLabel = [[NSMutableArray alloc]init];
    venueArray = [[NSMutableArray alloc]init];
    dateArray = [[NSMutableArray alloc]init];
    startTimeArray = [[NSMutableArray alloc]init];
    categoryArray = [[NSMutableArray alloc]init];
    endTimeArray = [[NSMutableArray alloc]init];
    eventIdArray = [[NSMutableArray alloc]init];
    eventViewsArray = [[NSMutableArray alloc]init];
    favoriteArray = [[NSMutableArray alloc]init];
    costArray = [[NSMutableArray alloc]init];
    descriptionArray = [[NSMutableArray alloc]init];

    for (NSDictionary *d  in  data) {
        
        NSString *imageString = d[@"image"];
        [imagesArray  addObject:imageString];
        
        NSString *titleText = d[@"title"];
        [titleLabel addObject:titleText];
        
        NSString *venueString = d[@"venue"];
        [venueArray  addObject:venueString];
        
        NSString *dateString = d[@"date"];
        [dateArray addObject:dateString];
        
        NSString *strtTimeString = d[@"start_time"];
        [startTimeArray  addObject:strtTimeString];
        
        NSString *endTimeString = d[@"end_time"];
        [endTimeArray  addObject:endTimeString];
        
        NSString *eventIdString = d[@"event_id"];
        [eventIdArray  addObject:eventIdString];
        
        NSString *eventViewsString = d[@"event_views"];
        [eventViewsArray  addObject:eventViewsString];
        
        NSString *favoriteString = d[@"favorite"];
        [favoriteArray  addObject:favoriteString];
        
        NSString *costString = d[@"cost"];
        [costArray  addObject:costString];
        
        NSString *descriptionString = d[@"description"];
        [descriptionArray addObject:descriptionString];
        
        NSMutableDictionary *response = [[[d valueForKey:@"categories"] objectAtIndex:0]mutableCopy];
        NSString *catgryString = [response valueForKey:@"cat_title"];
        
        
       // NSDictionary *cat = d[@"categories"];
       // NSString *catgryString = [cat valueForKeyPath:@"cat_title"];
        [categoryArray  addObject:catgryString];
        
        // NSLog(@"image valuesss :%@",imageString);
    }
    
    
    
//     for (NSDictionary *c  in  categories) {
//         
//         NSDictionary *cat = [c valueForKey: @"cat_title"];
//         //NSString *catString
//         [categoryArray addObject:cat];
//         //NSLog(@"category response ***** %@" , cat);
//     }
    //NSLog(@"category response ***** %@" , categoryArray);
    
    
    // NSLog(@"Images Data :%@",imagesArray);
    

    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //
    //    [defaults setObject:imagesArray forKey:@"imageKey"];
    //    [defaults synchronize];
    
    [_imagesTableView reloadData];
}





-(void)createTableView
{
  //  self.imagesTableView = [[UITableView alloc] initWithFrame: CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    //  self.edgesForExtendedLayout = UIRectEdgeAll;
    
    if (IS_IPHONE_6) {
        _imagesTableView.frame = CGRectMake(0, 102, self.view.frame.size.width, self.view.frame.size.height-102);
    }

    
    self.imagesTableView.delegate=self;
    self.imagesTableView.dataSource=self;
    self.imagesTableView.backgroundColor=[UIColor blackColor];
    self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:self.imagesTableView];
}



#pragma mark - tableview delegate methods ================= ================ ================ ================ ================


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return  300;
    }
    else{
        return  190;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return imagesArray.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell.layer.borderWidth = 5.0;
        cell.layer.borderColor = [UIColor colorWithRed:0.13 green:0.19 blue:0.25 alpha:1.0].CGColor;
        
        cell.imageView.image = nil;
        cell.textLabel.text = nil;
        
        UILabel *TitleTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+12, 70, 300, 50)];
        TitleTextLabel.tag = 101;
        [cell.contentView addSubview:TitleTextLabel];
        
        UILabel *dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+35, 95, 300, 50)];
        dateLabel.tag = 102;
        [cell.contentView addSubview:dateLabel];
        
        UILabel *startTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+115, 95, 300, 50)];
        startTimeLabel.tag = 103;
        [cell.contentView addSubview:startTimeLabel];
        
        UILabel *venueLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+35, 115, 283, 50)];
        venueLabel.tag = 104;
        [cell.contentView addSubview:venueLabel];
        
        if (IS_IPHONE_5)
        {
            rectCatText = CGRectMake(10, 155, 230,26);
            
        }else{
            rectCatText = CGRectMake(10, 155, 250,26);
        }
        UITextField *catField = [[UITextField alloc]initWithFrame:rectCatText];
        catField.tag = 1023;
        [cell.contentView addSubview:catField];
        
        
        if (IS_IPHONE_5)
        {
            rectText = CGRectMake(cell.frame.size.width - 50, 152, 60, 30);
        }else
        {
            rectText = CGRectMake(cell.frame.size.width - 20, 150, 80, 30);
        }
        UILabel *viewsLabel = [[UILabel alloc]initWithFrame:rectText];
        viewsLabel.tag = 106;
        [cell.contentView addSubview:viewsLabel];
        
        
        if (IS_IPHONE_5)
        {
            rectFavImage = CGRectMake(cell.frame.size.width - 60, 162, 23, 10);
        }else{
            rectFavImage = CGRectMake(cell.frame.size.width - 25, 160, 23, 10);
        }
        UIImageView *favImage = [[UIImageView alloc]initWithFrame:rectFavImage];
        favImage.tag = 1024;
        [cell.contentView addSubview:favImage];
        
        cell.tag = indexPath.row ;
        
    }
    
    
    NSString *arrayString = [NSString stringWithFormat:@"http://agiledevelopers.in/outnabout/assets/timthumb.php?src=http://agiledevelopers.in/outnabout/uploads/%@&h=320&w=500",[imagesArray objectAtIndex:indexPath.row]];

    NSURL *url =[NSURL URLWithString:arrayString];
   
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:url
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UITableViewCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                                    if (updateCell)
                                      //  updateCell.imageView.image = image;
                                    cell.backgroundView = [[UIImageView alloc] initWithImage:image];
                                    
                                    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, cell.frame.size.width, cell.frame.size.height)];
                                    CAGradientLayer *gradient = [CAGradientLayer layer];
                                    gradient.frame = view.bounds;
                                    
                                    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor colorWithRed:0.10 green:0.10 blue:0.10 alpha:1.0] CGColor], nil];
                                    [cell.backgroundView.layer insertSublayer:gradient atIndex:0];
                                });
                            }
                            [loadingView setHidden:YES];
                        }
      ];

    
    
    
    
    
    UIImageView *favImage = (UIImageView *)[cell viewWithTag:1024];
    favImage.image=[UIImage imageNamed:@"ic_view@3x.png"];
    
    UIImageView *timeImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_timer@3x.png"]];
    timeImage.frame = CGRectMake(12, 110, 15, 15);
    [cell.contentView addSubview:timeImage];
    
    UIImageView *locationImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_location@3x.png"]];
    locationImage.frame = CGRectMake(12, 130, 15, 18);
    [cell.contentView addSubview:locationImage];

    
    UILabel *TitleTextLabel = (UILabel *)[cell viewWithTag:101];
    TitleTextLabel.text = [NSString stringWithFormat:@"%@ ",[titleLabel objectAtIndex:indexPath.row]];
    [TitleTextLabel setTextColor:[UIColor whiteColor]];
    [TitleTextLabel setFont:[UIFont fontWithName:@"EuphemiaUCAS-Bold" size:20.0f]];
    
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:102];
    dateLabel.text = [NSString stringWithFormat:@"%@ | ",[dateArray objectAtIndex:indexPath.row]];
    [dateLabel setTextColor:[UIColor whiteColor]];
    [dateLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
    
    UILabel *startTimeLabel = (UILabel *)[cell viewWithTag:103];
    startTimeLabel.text = [NSString stringWithFormat:@"%@ ",[startTimeArray objectAtIndex:indexPath.row]];
    [startTimeLabel setTextColor:[UIColor whiteColor]];
    [startTimeLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
    
    UILabel *venueLabel = (UILabel *)[cell viewWithTag:104];
    venueLabel.text = [NSString stringWithFormat:@"%@ ",[venueArray objectAtIndex:indexPath.row]];
    [venueLabel setTextColor:[UIColor whiteColor]];
    [venueLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
    
    UITextField *catField=(UITextField *)[cell viewWithTag:1023];
    catField.text = [NSString stringWithFormat:@"%@  ",[categoryArray objectAtIndex:indexPath.row]];
    catField.borderStyle = UITextBorderStyleLine;
    catField.font= [UIFont systemFontOfSize:12.0f];
    catField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    catField.backgroundColor = [UIColor clearColor];
    [catField setAllowsEditingTextAttributes:NO];
    catField.textColor =[UIColor whiteColor];
    catField.layer.borderColor = [UIColor whiteColor].CGColor;
    catField.layer.borderWidth = 1.0f;

    UILabel *viewsLabel = (UILabel *)[cell viewWithTag:106];
    viewsLabel.text = [NSString stringWithFormat:@"%@ ",[eventViewsArray objectAtIndex:indexPath.row]];
    [viewsLabel setTextColor:[UIColor whiteColor]];
    viewsLabel.textAlignment = NSTextAlignmentCenter;
    viewsLabel.backgroundColor = [UIColor clearColor];
    [viewsLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:15.0f]];
    
    
//    UIView *_lineDiv=[[UIView alloc] init];
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        _lineDiv.frame=CGRectMake(0, 250.0f, self.view.frame.size.width, 5.0);
//    }else{
//        _lineDiv.frame=CGRectMake(0, 190.0f, self.view.frame.size.width, 1.0);
//        
//    }
//    _lineDiv.backgroundColor=[UIColor blackColor];
//    //_lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
//    [cell addSubview:_lineDiv];
    
    return cell;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SingleEventVC *singleEvent = [story instantiateViewControllerWithIdentifier:@"singleView"];
    
    singleEvent.singleTitle =[titleLabel objectAtIndex:indexPath.row];
    singleEvent.singleCategoryString =[categoryArray objectAtIndex:indexPath.row];
    singleEvent.costString =[costArray objectAtIndex:indexPath.row];
    singleEvent.eventIdString =[eventIdArray objectAtIndex:indexPath.row];
    singleEvent.descriptionString =[descriptionArray objectAtIndex:indexPath.row];
    singleEvent.venueString =[venueArray objectAtIndex:indexPath.row];
    singleEvent.endDateOrTimeString =[NSString stringWithFormat:@" End Date & Time: %@",[endTimeArray objectAtIndex:indexPath.row]];
    singleEvent.startDateOrTimeString =[NSString stringWithFormat:@" Start Date: %@ & Time: %@",[dateArray objectAtIndex:indexPath.row],[startTimeArray objectAtIndex:indexPath.row]];
    
    singleEvent.singleImageString =[NSString stringWithFormat:@"http://agiledevelopers.in/outnabout/assets/timthumb.php?src=http://agiledevelopers.in/outnabout/uploads/%@&h=320&w=480",[imagesArray objectAtIndex:indexPath.row]];
    
    
    [self.navigationController pushViewController:singleEvent animated:YES];
    
    
    // [NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:nil];
    
}


//- (IBAction)tappedRightButton:(id)sender
//{
//    
//    [self.tabBarController setSelectedIndex:1];
//    
//}



-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = CGRectMake(0, cell.contentView.frame.size.height/2 - 24, cell.frame.size.width, 120);
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor blackColor] CGColor],nil];
    
    [cell setBackgroundView:[[UIView alloc] init]];
    [cell.backgroundView.layer insertSublayer:grad atIndex:0];
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
