//
//  ViewController.m
//  Out N about
//
//  Created by Ram Kumar on 03/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "ViewController.h"
#import "SWRevealViewController.h"
#import "LoginVC.h"

#import "MainTabbarController.h"
#import "AllEventsTabBarVC.h"
#import "PaidEventTabbarVC.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


@interface ViewController ()

@end

@implementation ViewController
{
    NSArray *garbageArray;
    MainTabbarController *mainTabbarObject;
    UITabBarController *tabBars;
    PaidEventTabbarVC *greenViewController;
}
//@synthesize screenType;



//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        
//        // Custom initialization
//        
//    }
//    return self;
//}

- (void)viewDidLoad {
  [super viewDidLoad];

    
//    self.view.backgroundColor = [UIColor whiteColor];
//    self.title = @"All Events";
//    
//    garbageArray = [[NSArray alloc]initWithObjects:@"A",@"B",@"C", nil];
//
//    [self createTableView];
//    _barButtonItem.target = self.revealViewController;
//    _barButtonItem.action = @selector(revealToggle:);
//    
//    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//    
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *username = [defaults objectForKey:@"username"];
//    NSString *password = [defaults objectForKey:@"password"];
//    
//    NSLog(@"username1: %@  pass1 :%@",username,password);
//    
//    
//    
//  // use TAb bar controller main screen at position of ViewController==============================================================
//    mainTabbarObject = [[MainTabbarController alloc]init];
//    [mainTabbarObject setDelegate:self];
////    tabBars = [[UITabBarController alloc] init];
////    [tabBars setDelegate:self];
//    //UITabBar *tabBar = [[UITabBar alloc]init];
//    
//    NSMutableArray *localViewControllersArray = [[NSMutableArray alloc] initWithCapacity:2];
//    
//    UITabBarItem *localTabBarItem = [[UITabBarItem alloc]
//                                     initWithTitle:@"Date" image:nil tag:0];
// 
//    greenViewController = [[ThirdTabbarVC alloc] initWithNibName:nil bundle:nil];
//    greenViewController.tabBarItem.title=@"black";
//    greenViewController.tabBarItem.tag = 0;
//    [greenViewController setTabBarItem:localTabBarItem];
//
// //   greenViewController.view.backgroundColor = [UIColor yellowColor];
//   
//    UITabBarItem *localTabBarItem2 = [[UITabBarItem alloc]
//                                     initWithTitle:@"time" image:nil tag:1];
//
//    AllEventsTabBarVC *blueViewController = [[SecondTabBarVC alloc] initWithNibName:nil bundle:nil];
//    blueViewController.tabBarItem.title=@"Blue";
//    blueViewController.tabBarItem.tag = 1;
//    [blueViewController setTabBarItem:localTabBarItem2];
//
//  //  blueViewController.view.backgroundColor = [UIColor greenColor];
//    
//    
//    [localViewControllersArray addObject:greenViewController];
//    [localViewControllersArray addObject:blueViewController];
//    
//    [[UITabBar appearance] setBackgroundColor:[UIColor yellowColor]];
//    mainTabbarObject.viewControllers = localViewControllersArray;
//    mainTabbarObject.view.autoresizingMask=(UIViewAutoresizingFlexibleHeight);
//    [mainTabbarObject setSelectedIndex:1];

//    tabBars.viewControllers = localViewControllersArray;
//    tabBars.view.autoresizingMask=(UIViewAutoresizingFlexibleHeight);
//    [tabBars setSelectedIndex:0];
    
 //   [self.view addSubview:mainTabbarObject.view];

}



//#pragma mark - Tabbar Controller delegates ============================================================================================
//
//
//- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
//{
//    if (tabBarController==mainTabbarObject) {
//   
//    if(viewController.tabBarItem.tag==0)
//    {
//        //your code
//        NSLog(@"click black");
//    }
//    else if(viewController.tabBarItem.tag==1)
//    {
//        //your code
//        NSLog(@"click green");
//        
//    }
//    }
//}
//
//
//-(void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//
//    
//    if ([screenType isEqualToString:@"LogoutNav"]) {
//        NSLog(@"logout clicked");
//        
//        UIAlertController * alert=   [UIAlertController
//                                      alertControllerWithTitle:@"Message"
//                                      message:@"You really want to logout."
//                                      preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction* okButton = [UIAlertAction
//                                   actionWithTitle:@"OK"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//                                       
//                                      [self resetDefaults];
//                                      [self performSegueWithIdentifier:@"tologin" sender:nil];
//                                       
//                                      // [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
//                                       
//                                      screenType = nil;
//                                       
//                                       
//                                   }];
//        UIAlertAction* cancelButton = [UIAlertAction
//                                       actionWithTitle:@"Cancel"
//                                       style:UIAlertActionStyleDefault
//                                       handler:^(UIAlertAction * action)
//                                       {
//                                           //Handel your yes please button action here
//                                           
//                                           
//                                       }];
//        
//        [alert addAction:okButton];
//        [alert addAction:cancelButton];
//        
//        
//        [self presentViewController:alert animated:YES completion:nil];
//    }
//    
//    else{
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        
//        int loggedIn = (int)[defaults integerForKey:@"isloggedIn"];
//        if (loggedIn !=1) {
//            [self performSegueWithIdentifier:@"tologin" sender:nil];
//
//
//        }
//        else{
//          //  [self performSegueWithIdentifier:@"toTabbar" sender:nil];
//
//             NSString *username = [defaults objectForKey:@"username"];
//            _nameLabel.text = username;
//        }
//        
//        
//    }
//
//}
//
//- (void)resetDefaults {
//    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
//   // [defs removeObjectForKey:@"username"];
////    [defs removeObjectForKey:@"password"];
//    NSDictionary * dict = [defs dictionaryRepresentation];
//    for (id key in dict) {
//        [defs removeObjectForKey:key];
//    }
//    [defs synchronize];
//}
//
//
//
//-(void)createTableView
//{
//    self.imagesTableView = [[UITableView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
//    
//    self.imagesTableView.delegate=self;
//    self.imagesTableView.dataSource=self;
//    self.imagesTableView.backgroundColor=[UIColor redColor];
//   // self.imagesTableView.rowHeight = IS_IPAD ? 60 : 50.0f;
//    self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.imagesTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
//    
//  [self.view addSubview:self.imagesTableView];
//}
//
//
//
//#pragma mark - tableview delegate methods ================= ================ ================ ================ ================
//
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
// 
//        return  160;
//
//}
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    
//      return garbageArray.count;
//    
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    if (cell == nil)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.textLabel.backgroundColor = [UIColor clearColor];
//    }
//    
//    cell.textLabel.text = [garbageArray objectAtIndex:indexPath.row];
//    UIView *_lineDiv=[[UIView alloc] init];
//    _lineDiv.frame=CGRectMake(0, 160.0f, self.view.frame.size.width, 1.0);
//    _lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
//    [cell addSubview:_lineDiv];
//
//    return cell;
//
//}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
