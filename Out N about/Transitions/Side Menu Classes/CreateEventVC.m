//
//  CreateEventVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 29/06/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "CreateEventVC.h"
#import "SWRevealViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>


@interface CreateEventVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate>


//@property (nonatomic, strong) UIImagePickerController *mediaPicker;
@property (nonatomic, strong) NSString *selectedImagePath;

@property (nonatomic, strong) UIImage *selectedImage;

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs ((double)[[UIScreen mainScreen] bounds].size.height - (double) 667) < DBL_EPSILON)

#define API_URL @"http://agiledevelopers.in/outnabout/webservices/users/"


@end

@implementation CreateEventVC
{
    UIImagePickerController *mediaPicker;
    NSMutableDictionary *dictImages;
    
    NSString *imageName;
    UIImage *imagePath;
    //UIDatePicker *datePicker;
}
@synthesize selectedItems;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if (IS_IPHONE_5) {
        _scrollView.contentSize =CGSizeMake(320,800);

    }else
    {
    _scrollView.contentSize =CGSizeMake(320, 900);
    }
    _scrollView.bounces = NO;
//    _scrollView.showsHorizontalScrollIndicator = NO;
//    _scrollView.scrollEnabled = YES;
    
    
    //_galleryImageView.image = [UIImage imageNamed:@"ic_bg@3x.png"];
    
    
    _barButtonItem = [[UIBarButtonItem alloc]init];
    [_barButtonItem setImage:[UIImage imageNamed:@"ic_menu@3x.png"]];
    _barButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = _barButtonItem;
    
    _rightBarButtonItem = [[UIBarButtonItem alloc]init];
    _rightBarButtonItem.title = @"Save";
    _rightBarButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = _rightBarButtonItem;

    _barButtonItem.target = self.revealViewController;
    _barButtonItem.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    selectedItems = [[NSMutableArray alloc]init];
    [selectedItems addObject:@"1"];
    [selectedItems addObject:@"2"];
    [selectedItems addObject:@"3"];
    [selectedItems addObject:@"4"];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    [_categoriesTF setInputView:datePicker];

    
//        UITapGestureRecognizer *swipeLeft = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(af)];
//      //  [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
//        [_categoriesTF.superview addGestureRecognizer:swipeLeft];
    
}




-(void)af{
    
    
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    [_categoriesTF setInputView:datePicker];
    
}


-(void) dateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)_categoriesTF.inputView;
    [picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    _categoriesTF.text = [NSString stringWithFormat:@"%@",dateString];
}






- (IBAction)galleryButtonAction:(id)sender
{
    
    
    mediaPicker = [[UIImagePickerController alloc] init];
    mediaPicker.delegate = self;
    mediaPicker.allowsEditing = NO;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take photo", @"Choose existing", nil];
    [actionSheet showInView:self.view];

    
    
//    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
//    imagePickerController.delegate = self;
//    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
//    [self presentModalViewController:imagePickerController animated:YES];
    
}







-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        
        
        
//        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//            return;
//        }
        mediaPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        
        
        
        
    } else if (buttonIndex == 1) {
        mediaPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    else {
        return;
    }
    [self presentViewController:mediaPicker animated:YES completion:nil];
}









- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    dictImages=[[NSMutableDictionary alloc]init];
    
    // [KiiViewUtilities showProgressHUD:@"Processing..." withView:self.view];
    
    imagePath = [info valueForKey: UIImagePickerControllerOriginalImage];
    
    UIImagePickerControllerSourceType sourceType = picker.sourceType;
    
    ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
    
    imagePath = [info valueForKey: UIImagePickerControllerOriginalImage];
    
    NSString *docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath =  [docDirPath stringByAppendingPathComponent:@"myImage.png"];
    NSLog (@"File Path = %@", filePath);
    
    
    imageName = [NSString stringWithFormat:@"%0.0f.jpg",[[NSDate date] timeIntervalSince1970]];
    
    [dictImages setObject:imagePath forKey:@"profilepic"];
    
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        // Request to save the image to camera roll
        [assetsLibrary writeImageToSavedPhotosAlbum:[image CGImage] orientation:(ALAssetOrientation) [image imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error) {
            if (error) {
                //                [KiiViewUtilities hideProgressHUD:self.view];
                //                [KiiViewUtilities showFailureHUD:@"Selected photo can not be used" withView:self.view];
            } else {
                NSLog(@"url %@", assetURL);
                
                [self saveImageDataToTemporaryArea:assetsLibrary withAssetUrl:assetURL];
            }
        }];
        
        _galleryImageView.image= nil;
        _galleryImageView = nil;
        
    } else {
        NSURL *url = (NSURL *) [info valueForKey:UIImagePickerControllerReferenceURL];
        
        
        [self saveImageDataToTemporaryArea:assetsLibrary withAssetUrl:url];
    }
}










-(void)saveImageDataToTemporaryArea:(ALAssetsLibrary *)assetsLibrary withAssetUrl:(NSURL *)url
{
    [assetsLibrary assetForURL:url
                   resultBlock:^(ALAsset *asset) {
                       ALAssetRepresentation *representation = [asset defaultRepresentation];
                       NSString *fileUTI = [representation UTI];
                       NSString *fileName = [representation filename];
                       ALAssetOrientation orientation = (ALAssetOrientation) [[asset valueForProperty:@"ALAssetPropertyOrientation"] intValue];
                       CGImageRef cgImg = [representation fullResolutionImage];
                       self.selectedImage = [UIImage imageWithCGImage:cgImg scale:1.0 orientation:(UIImageOrientation) orientation];
                       NSData *data = nil;
                       if ([fileUTI isEqualToString:@"public.png"]) {
                           data = UIImagePNGRepresentation(self.selectedImage);
                       } else {
                           data = UIImageJPEGRepresentation(self.selectedImage, 95);
                       }
                       
                       NSError *error = nil;
                       self.selectedImagePath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
                       [data writeToFile:self.selectedImagePath options:NSDataWritingAtomic error:&error];
                       if (error != nil) {
                           self.selectedImage = nil;
                           self.galleryImageView = nil;
                           self.galleryImageView.image = nil;
                           NSLog(@"Failed to save : %i - %s", errno, strerror(errno));
                           //                           [KiiViewUtilities hideProgressHUD:self.view];
                           //                           [KiiViewUtilities showFailureHUD:@"Selected photo can not be used" withView:self.view];
                           return;
                       }
                       self.galleryImageView.contentMode = UIViewContentModeScaleAspectFit;
                       self.galleryImageView.image = self.selectedImage;
                       
                       //                       [KiiViewUtilities hideProgressHUD:self.view];
                       //                       [KiiViewUtilities showSuccessHUD:@"Photo selected" withView:self.view];
                   }
                  failureBlock:^(NSError *error) {
                      //                      [KiiViewUtilities hideProgressHUD:self.view];
                      //                      [KiiViewUtilities showFailureHUD:@"Selected photo can not be used" withView:self.view];
                  }];
}







- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}








//- (void)imagePickerController:(UIImagePickerController *)picker
//        didFinishPickingImage:(UIImage *)image
//                  editingInfo:(NSDictionary *)editingInfo
//{
//    // Dismiss the image selection, hide the picker and
//    
//    //show the image view with the picked image
//    
//    UIImage *image1 = [editingInfo valueForKey:UIImagePickerControllerOriginalImage];
//    //Or you can get the image url from AssetsLibrary
//    NSURL *path = [editingInfo valueForKey:UIImagePickerControllerReferenceURL];
//    
//    [picker dismissViewControllerAnimated:YES completion:nil];
//    //UIImage *newImage = image;
//    
//    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.png"];
//    UIImage *images = _galleryImageView.image; // imageView is my image from camera
//    NSData *imageData = UIImagePNGRepresentation(images);
//    [imageData writeToFile:savedImagePath atomically:NO];
//}





-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
    [_scrollView endEditing:YES];
}




- (IBAction)getEventLocation:(id)sender {
    
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}



@end
