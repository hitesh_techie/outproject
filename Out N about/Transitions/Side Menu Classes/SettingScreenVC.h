//
//  SettingScreenVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 21/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingScreenVC : UIViewController

@property (strong, nonatomic) IBOutlet UIBarButtonItem *barButtonItem;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
