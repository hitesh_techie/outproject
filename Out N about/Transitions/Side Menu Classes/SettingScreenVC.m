//
//  SettingScreenVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 21/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "SettingScreenVC.h"
#import "ALToastView.h"
#import "SWRevealViewController.h"

@interface SettingScreenVC ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation SettingScreenVC
{
    NSArray *profileArray;
    NSArray *notifArray;
    NSArray *socialAccArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    profileArray = @[@"Edit profile",@"Preferences",@"Cities",@"Clear cache"];
    notifArray = @[@"Enable push notifications"];
    socialAccArray = @[@"About",@"Terms of use",@"Privacy policy",@"Tell a friend",@"Feedback",@"Signout"];
    
    
    _barButtonItem = [[UIBarButtonItem alloc]init];
    [_barButtonItem setImage:[UIImage imageNamed:@"ic_menu@3x.png"]];
    _barButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = _barButtonItem;

    _barButtonItem.target = self.revealViewController;
    _barButtonItem.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    UILabel* titleLabelNav = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
    titleLabelNav.textAlignment = NSTextAlignmentLeft;
    titleLabelNav.text = NSLocalizedString(@"Settings",@"");
    titleLabelNav.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabelNav;


    [self createTableview];
}



-(void)createTableview
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, self.view.frame.size.height-65) style:UITableViewStyleGrouped];

    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0)
    {
        return [profileArray count];
    }
    else if (section==1){
        return [notifArray count];
    }
    else{
         return [socialAccArray count];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return @"Profile";
    else if(section == 1)
        return @"Notifications";
    else
        return @"Social Accounts";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        
        
        
    }
    
    
    if (indexPath.section==0) {
  
        cell.textLabel.text = [profileArray objectAtIndex:indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (indexPath.section==1) {
        cell.textLabel.text = [notifArray objectAtIndex:indexPath.row];
        
        UIButton *tickButton = [UIButton buttonWithType:UIButtonTypeCustom];
        tickButton.frame = CGRectMake(320, 13, 20, 20);
        [tickButton setImage:[UIImage imageNamed:@"ic_Select_enable@3x.png"] forState:UIControlStateNormal];
        //[tickButton addTarget:self action:@selector(incl) forControlEvents:UIControlStateNormal];
        [cell.contentView addSubview:tickButton];
        
    }
    else{
        cell.textLabel.text = [socialAccArray objectAtIndex:indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }
    
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}



@end
