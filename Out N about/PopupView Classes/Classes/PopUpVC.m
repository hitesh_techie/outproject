//
//  PopUpVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 15/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "PopUpVC.h"
#import "SingleEventVC.h"
#import "BIZPopupViewController.h"

@interface PopUpVC ()<UITextViewDelegate>

#define API_URL @"http://agiledevelopers.in/outnabout/webservices/users/"

@end

@implementation PopUpVC

{
    NSString *access_token;
    NSString *ratingValue;
    BIZPopupViewController *BIZPopupViewControllerObject;
}
@synthesize rateView,statusLabel,commentTV,submitButton,eventIdString;


- (void)viewDidLoad {
    [super viewDidLoad];
     [commentTV setDelegate:self];
    // Do any additional setup after loading the view.
    NSLog(@"single event is  %@", eventIdString);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    access_token = [defaults objectForKey:@"access_token"];

    
    self.rateView.notSelectedImage = [UIImage imageNamed:@"kermit_empty.png"];
    self.rateView.halfSelectedImage = [UIImage imageNamed:@"kermit_half.png"];
    self.rateView.fullSelectedImage = [UIImage imageNamed:@"kermit_full.png"];
    self.rateView.rating = 0;
    self.rateView.editable = YES;
    self.rateView.maxRating = 5;
    self.rateView.delegate = self;

    
}


- (void)viewDidUnload
{
    [self setRateView:nil];
    [self setStatusLabel:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}





- (IBAction)commentAndRatingSubmitAction:(id)sender
{
    if ([commentTV.text isEqualToString:@""]) {
        [[[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please enter comment." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
    else if  (!ratingValue.length) {
            [[[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please enter rating." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        }
    
        
    else {
    [self postMethodForFavorite];
    //[self dismissPopupViewControllerAnimatedPassiveAction];
    [self dismissViewControllerAnimated:YES completion:nil];
    }
    NSLog(@"comment value *****%@     RATING %@",commentTV.text,self.statusLabel.text);
}


//- (void)dismissPopupViewControllerAnimatedPassiveAction
//{
//    [self dismissPopupViewControllerAnimated:YES];
//}
//     
//     - (void)dismissPopupViewControllerAnimated:(BOOL)animated
//    {
//        [BIZPopupViewControllerObject dismissPopupViewControllerAnimated:animated completion:nil];
//    }


- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
    self.statusLabel.text = [NSString stringWithFormat:@"Rating: %f", rating];
    ratingValue = [NSString stringWithFormat:@"%f", rating];
    
}


-(void)postMethodForFavorite
{
    
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"commentsAndRating")];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    
    NSString *postData = [NSString stringWithFormat:@"event_id=%@ & app_token=%@ & comment=%@ & rating=%@", eventIdString,access_token,commentTV.text,ratingValue];
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
    
    
    
}


#pragma mark - NSURL Post methods 


-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"eeeeeeee  %@" , error);
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];
    
    NSString *msg = jsonArray[@"msg"];
    
    [[[UIAlertView alloc]initWithTitle:@"Submited" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];

    //NSLog(@"json response is :%@",jsonArray);
}





- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}




-(void)textViewDidBeginEditing:(UITextView *)textView
{
    _enterCommentLabel.hidden = YES;
    
}

//-(void)textViewDidChange:(UITextView *)textView
//{
//    _enterCommentLabel.hidden = YES;
//
//}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if ([commentTV.text isEqualToString:@""]) {
        _enterCommentLabel.hidden = NO;

    }else{
    _enterCommentLabel.hidden = YES;
    }
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}



@end
