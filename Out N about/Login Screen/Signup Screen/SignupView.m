//
//  SignupView.m
//  Out N about
//
//  Created by Ram Kumar on 03/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "SignupView.h"
#import "LoginVC.h"

@interface SignupView ()<NSURLConnectionDataDelegate>

#define API_URL @"http://agiledevelopers.in/outnabout/webservices/users/"
#define kOFFSET_FOR_KEYBOARD 80.0
@end

@implementation SignupView

{
    UIAlertController * alertForFields;
    
    NSPredicate *emailTest;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    
    [_firstNameTF.layer setCornerRadius:5.0f];
    [_lastNameTF.layer setCornerRadius:5.0f];
    [_emailTF.layer setCornerRadius:5.0f];
    [_passwordTF.layer setCornerRadius:5.0f];
    [_confirmPassTF.layer setCornerRadius:5.0f];
    
    UIImageView *backgroundImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [backgroundImage setImage:[UIImage imageNamed:@"login_bg.jpg"]];
    [self.view insertSubview:backgroundImage atIndex:0];
    
    

    
    
}


- (IBAction)sendButtonAction:(id)sender {
    
}



- (IBAction)registerWithNewUserAction:(id)sender
{
    
    BOOL isValidString = [self checkTextfieldValidations];
    if (isValidString) {
        [self postMethod];
    }
    
    
}






-(BOOL)checkTextfieldValidations{
    Boolean isValid = true;
    
    if ([_firstNameTF.text isEqualToString:@""]) {
        isValid = false;
        
        alertForFields=   [UIAlertController
                           alertControllerWithTitle:@""
                           message:@"First name cant't be empty!"
                           preferredStyle:UIAlertControllerStyleAlert];
        
    } else if ([_lastNameTF.text isEqualToString:@""]) {
        
        isValid = false;
        
        alertForFields=   [UIAlertController
                           alertControllerWithTitle:@""
                           message:@"Last name can't be empty!"
                           preferredStyle:UIAlertControllerStyleAlert];
    }
    

    
    else if ([_emailTF.text isEqualToString:@""] ) {
        
        isValid = false;
        
        alertForFields=   [UIAlertController
                           alertControllerWithTitle:@""
                           message:@"Email can't be empty!"
                           preferredStyle:UIAlertControllerStyleAlert];
    }
    
    
    else if ([emailTest evaluateWithObject:_emailTF.text] == NO ) {
        
        isValid = false;
        
        alertForFields=   [UIAlertController
                           alertControllerWithTitle:@""
                           message:@"Invalid email address!"
                           preferredStyle:UIAlertControllerStyleAlert];
    }
     else if ([_passwordTF.text isEqualToString:@""]) {
         
         isValid = false;
         
         alertForFields=   [UIAlertController
                            alertControllerWithTitle:@""
                            message:@"Password can't be empty!"
                            preferredStyle:UIAlertControllerStyleAlert];
     }
     else if ([_confirmPassTF.text isEqualToString:@""]) {
         
         isValid = false;
         
         alertForFields=   [UIAlertController
                            alertControllerWithTitle:@""
                            message:@"Confirm password can't be empty!"
                            preferredStyle:UIAlertControllerStyleAlert];
     }

    
     else if (![_passwordTF.text isEqualToString:_confirmPassTF.text]) {
         
         isValid = false;
         
         alertForFields=   [UIAlertController
                            alertControllerWithTitle:@""
                            message:@"Password did not match!"
                            preferredStyle:UIAlertControllerStyleAlert];
     }
    
    UIAlertAction* okButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                                    style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                                                {
                                                                      //Handel your yes please button action here
                               
                               
                                                    }];
                                       [alertForFields addAction:okButton];
                                       
                                       [self presentViewController:alertForFields animated:YES completion:nil];

    
    return isValid;
}






#pragma mark - post method to create new user 


-(void)postMethod
{
    
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"signup")];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
   NSString *postData = [NSString stringWithFormat:@"firstname=%@ & lastname=%@ & email=%@ &  password=%@", _firstNameTF.text,_lastNameTF.text,_emailTF.text,_passwordTF.text];
    //   NSLog(@"username****: %@  pass**** :%@",usernameTF.text,passwordTF.text);
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
    
}




#pragma mark - NSURL Post methods 


-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"eeeeeeee  %@" , error);
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //initialize convert the received data to string with UTF8 encoding
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse
    NSLog(@"response :%@",jsonArray);
    
    
    [[[UIAlertView alloc]initWithTitle:@"Message" message:jsonArray[@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    
    //NSLog(@"dataaaaaaaa %@",jsonArray);  //here is your output

}

- (IBAction)loginViewAction:(id)sender {
    
   // [self performSegueWithIdentifier:@"tologin" sender:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:_emailTF] || [sender isEqual:_firstNameTF] || [sender isEqual:_lastNameTF] || [sender isEqual:_passwordTF] || [sender isEqual:_confirmPassTF])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}






-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}



-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}





-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_firstNameTF becomeFirstResponder];
    [_lastNameTF becomeFirstResponder];

    [_emailTF becomeFirstResponder];

    [_passwordTF becomeFirstResponder];
    [_confirmPassTF becomeFirstResponder];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
