//
//  main.m
//  Out N about
//
//  Created by Ram Kumar on 03/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
